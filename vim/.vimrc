set nocompatible
set smartcase
set nowrap

syntax on
filetype plugin on

set showmatch
set ts=4
set sts=4
set sw=4
set autoindent
set smartindent
set smarttab
set expandtab
set number
set path+=**
set wildmenu
set complete-=i
set clipboard+=unnamedplus
set termguicolors

set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 15
let g:netrw_liststyle = 3

let mapleader = " "

" Plugins

call plug#begin('~/.local/share/nvim/plugged')

Plug 'rust-lang/rust.vim'

Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

Plug 'dense-analysis/ale'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'icymind/neosolarized'
Plug 'sainnhe/forest-night'

Plug 'jiangmiao/auto-pairs'

Plug '/usr/bin/fzf'
Plug 'junegunn/fzf.vim'

Plug 'tpope/vim-fugitive'

Plug 'jceb/vim-orgmode'
Plug 'vim-scripts/utl.vim'
Plug 'lervag/vimtex'

Plug 'voldikss/vim-floaterm'

call plug#end()

colorscheme NeoSolarized

let g:airline_theme='monochrome'

let g:ale_set_highlights = 0
let g:lsp_highlights_enabled = 0
let g:lsp_virtual_text_enabled = 0

nmap gd :LspDefinition<CR>

nnoremap <leader>lk :LspHover<CR>
nnoremap <leader>ld :LspDefinition<CR>

" Java quick stuff
autocmd FileType java nmap <buffer> <leader>pc :!./gradlew check<CR>
autocmd FileType java nmap <buffer> <leader>pb :!./gradlew build<CR>
autocmd FileType java nmap <buffer> <leader>pr :!./gradlew run<CR>

" Rust quick stuff
autocmd FileType rust nmap <buffer> <leader>pc :!cargo check<CR>
autocmd FileType rust nmap <buffer> <leader>pb :!cargo build<CR>
autocmd FileType rust nmap <buffer> <leader>pr :!cargo run<CR>

" Fuzzy Finding 
nnoremap <leader><leader> :FZF<CR>

" Tagbar and helpful side-pane functions
nmap <C-t> :TagbarToggle<CR>
map <C-n> :Lexplore<CR>

" Fugitive
nmap <silent> <leader>gg :Gstatus<CR>
nmap <silent> <leader>gc :Gcommit<CR>
nmap <silent> <leader>gb :Gblame<CR>
nmap <silent> <leader>gu :Gpull<CR>
nmap <silent> <leader>gp :Gpush<CR>

" Fecking spell check
nnoremap <leader>S :split<CR>:term aspell --lang=fi check %<CR>

"Window movement and manipulation
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

nmap <silent> <leader>ws :split<CR>
nmap <silent> <leader>wv :vsplit<CR>

nmap <silent> <leader>wk :wincmd k<CR>
nmap <silent> <leader>wj :wincmd j<CR>
nmap <silent> <leader>wh :wincmd h<CR>
nmap <silent> <leader>wl :wincmd l<CR>

nmap <silent> <leader>wq :q<CR>
nmap <silent> <A-S-l> :vertical resize -5<CR>
nmap <silent> <A-S-h> :vertical resize +5<CR>
nmap <silent> <A-S-j> :resize +5<CR>
nmap <silent> <A-S-k> :resize -5<CR>

" Buffers
nmap <silent> <leader>bk :bd<CR>

" Moving back and forth in tabs
nmap <silent> <leader><TAB>h gT
nmap <silent> <leader><TAB>l gt
nmap <silent> <leader><TAB>n :tabnew<CR>
nmap <silent> <leader><TAB>d :tabclose<CR>

nmap <silent> <C-l> gt
nmap <silent> <C-h> gT

" Quick escape from terminal mode
tnoremap <Esc><Esc> <C-\><C-n>
nmap <silent> <A-CR> :term<CR>:set nonu<CR>
nmap <silent> <leader>oT :term<CR>:set nonu<CR>
nmap <silent> <leader>ot :FloatermNew --height=0.4 --width=0.4 --wintype=normal<CR><ESC><ESC>:set nonu<CR>a

" Generic copy and paste controls
vnoremap <C-c> "+y
vnoremap <C-x> "+d
map <C-v> "+P
map <C-s> :w<CR>
