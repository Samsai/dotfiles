;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

;; THEMING
(setq doom-theme 'doom-gruvbox)

;; MISC
(add-to-list 'load-path "~/.doom.d")
(global-auto-revert-mode t)

(xclip-mode 1)

(setq browse-url-browser-function 'eww-browse-url)


;; GENERAL KEYBINDS
(map! :leader
      :desc "Spell check a word/region" "S" #'ispell-word)

(map! :leader
      :desc "Spell check a word/region" "B" 'my/browse-url)

(map! :leader
      :desc "Increase text size" "+" #'text-scale-increase
      :desc "Decrease text size" "-" #'text-scale-decrease)

(map! :leader
      (:after lsp-mode
      (:prefix-map ("l" . "LSP")
       :desc "Describe" "d" #'lsp-describe-thing-at-point
       :desc "Restart" "r" #'lsp-workspace-restart)))

;; TOOLS

;; ERC
(use-package! erc
  :custom
        (erc-nick "Samsai")
        (erc-server "chat.freenode.net")
        (erc-modules '(autojoin button completion fill irccontrols list match menu move-to-prompt netsplit networks noncommands notifications readonly ring stamp track))
        (erc-lurker-hide-list '("JOIN" "PART" "QUIT")))

(defun erc-cmd-BAN (nick)
  (let* ((chan (erc-default-target))
         (who (erc-get-server-user nick))
         (host (erc-server-user-host who))
         (user (erc-server-user-login who)))
    (erc-send-command (format "MODE %s +b *!%s@%s" chan user host))))

(defun erc-cmd-KICKBAN (nick &rest reason)
  (setq reason (mapconcat #'identity reason " "))
  (and (string= reason "")
       (setq reason nil))
  (erc-cmd-BAN nick)
  (erc-send-command (format "KICK %s %s %s"
                            (erc-default-target)
                            nick
                            (or reason
                                "Kicked (kickban)"))))

;; Circe

(after! circe
  (set-irc-server! "irc.libera.chat"
    `(:tls t
      :port 6697
      :nick "Samsai"
      :sasl-username "Samsai"
      :sasl-password (lambda (&rest _) (+pass-get-secret "irc/libera.chat"))
      :channels ("#gamingonlinux")))
  (set-irc-server! "irc.freegamedev.net"
    `(:tls t
      :port 6697
      :nick "Samsai"
      :sasl-username "Samsai"
      :sasl-password (lambda (&rest _) (+pass-get-secret "irc/freegamedev.net"))
      :channels ("#general")
      )))

(setq circe-reduce-lurker-spam t)

(defun circe-command-KICK (args)
    "Kick a user from the current channel using ChanServ."

     (circe-command-MSG "ChanServ"
                         (format "KICK %s %s"
                                 circe-chat-target
                                 args)))

(defun circe-command-BAN (args)
    "Ban a user from the current channel using ChanServ."

     (circe-command-MSG "ChanServ"
                         (format "BAN %s %s"
                                 circe-chat-target
                                 args)))

;; Highlight nicknames in ERC
(use-package! erc-hl-nicks
  :after erc)

;; mu4e

(use-package! mu4e
  :defer t
  :config
        (setq user-mail-address "samsai@posteo.net")
        (use-package! smtpmail
        :defer t)
        (setq message-send-mail-function 'smtpmail-send-it
        smtpmail-smtp-user           "samsai@posteo.net"
        smtpmail-default-smtp-server "posteo.de"
        smtpmail-smtp-server         "posteo.de"
        smtpmail-smtp-service        587
        smtpmail-stream-type         'starttls
        smtpmail-use-gnutls          t))

(set-email-account! "Samsai"
  '((mu4e-sent-folder       . "/Samsai/Sent")
    (mu4e-drafts-folder     . "/Samsai/Drafts")
    (mu4e-trash-folder      . "/Samsai/Trash")
    (mu4e-refile-folder     . "/Samsai/Archive")
    (smtpmail-smtp-user     . "samsai@posteo.net")
    (user-mail-address      . "samsai@posteo.net"))
  t)

;; EWW
;; Mimicking Elpher keybindings

(after! eww
  (map! :map eww-mode-map
        :n "u" #'eww-back-url
        :n "R" #'eww-reload
        :n "e" #'eww-browse-with-external-browser
        :n "B" #'eww-list-bookmarks
        :n "a" #'eww-add-bookmark
        :n "D" #'eww-download
        :n "<tab>" #'shr-next-link
        :n "S-<tab>" #'shr-previous-link
        :n "Y" #'eww-copy-page-url
        :n "g" #'eww))

;; ELPHER
(after! (elpher)
   (setq elpher-use-tls t)
   (setq gnutls-verify-error 'nil)
   (map! :map evil-motion-state-map
         :g "<tab>" 'nil))



;; ELFEED

(setq rmh-elfeed-org-files (list "~/org/elfeed.org"))

(use-package! elfeed
  :defer t
  :config
  (setq
   elfeed-search-filter "@15-days-ago +unread"
   elfeed-use-curl 't)
  (elfeed-org))

(after! elfeed
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :before "2 weeks ago"
                                :remove 'unread)))

;; Elfeed keybindings
(defun my/elfeed-mark-all-as-read ()
  (interactive)
  (mark-whole-buffer)
  (elfeed-search-untag-all-unread))

(defun my/elfeed-search-browse-url-eww ()
  (interactive)
  (elfeed-search-yank)
  (let ((url (current-kill 0)))
    (eww url)))

(after! elfeed
        (map! :map elfeed-search-mode-map
                :n "U" #'elfeed-update
                :n "b" #'elfeed-search-browse-url
                :n "e" #'my/elfeed-search-browse-url-eww
                :n "y" #'elfeed-search-yank
                :n "/" #'elfeed-search-set-filter
                :n "a" #'elfeed-search-untag-all-unread
                :n "A" #'my/elfeed-mark-all-as-read))

;; DIRED

;; Dired keybinds resembling Ranger
(defun my/dired-quit-all ()
  "Quit all Dired buffers and close frame"
  (interactive)
  (+dired/quit-all)
  (evil-quit))

(map! :map dired-mode-map
      :n "l" #'dired-find-file
      :n "h" #'dired-up-directory
      :n "y" #'dired-do-copy
      :n "Y" #'dired-copy-filename-as-kill
      :n "d" #'dired-do-rename
      :n "x" #'dired-do-delete
      :n "A" #'dired-toggle-marks)

;; ORG

(custom-set-variables
 '(org-agenda-files (list org-directory))
 '(org-directory "~/org"))

;; Org-mode keybinds
(after! org
  (map! :map org-mode-map
        :n "M-j" #'org-metadown
        :n "M-k" #'org-metaup)
  )

;; Rust lang

(setq rustic-flycheck-clippy-params "--message-format=json")

;; Javascript

(setq js-indent-level 2)

;; Soft-wrap gemini files with visual-fill-column-mode
(add-hook! gemini-mode
           (visual-fill-column-mode))
