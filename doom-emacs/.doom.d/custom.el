(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("79278310dd6cacf2d2f491063c4ab8b129fee2a498e4c25912ddaa6c3c5b621e" "0685ffa6c9f1324721659a9cd5a8931f4bb64efae9ce43a3dba3801e9412b4d8" "be9645aaa8c11f76a10bcf36aaf83f54f4587ced1b9b679b55639c87404e2499" "fce3524887a0994f8b9b047aef9cc4cc017c5a93a5fb1f84d300391fba313743" "01cf34eca93938925143f402c2e6141f03abb341f27d1c2dba3d50af9357ce70" "75b8719c741c6d7afa290e0bb394d809f0cc62045b93e1d66cd646907f8e6d43" "e6ff132edb1bfa0645e2ba032c44ce94a3bd3c15e3929cdf6c049802cf059a2a" "76bfa9318742342233d8b0b42e824130b3a50dcc732866ff8e47366aed69de11" "8f5a7a9a3c510ef9cbb88e600c0b4c53cdcdb502cfe3eb50040b7e13c6f4e78e" "0a41da554c41c9169bdaba5745468608706c9046231bbbc0d155af1a12f32271" "aaa4c36ce00e572784d424554dcc9641c82d1155370770e231e10c649b59a074" "ca70827910547eb99368db50ac94556bbd194b7e8311cfbdbdcad8da65e803be" "4bca89c1004e24981c840d3a32755bf859a6910c65b829d9441814000cf6c3d0" "d5a878172795c45441efcd84b20a14f553e7e96366a163f742b95d65a3f55d71" default))
 '(erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
 '(erc-nick "Samsai")
 '(org-agenda-files (list org-directory))
 '(org-directory "~/org"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
