;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)

;; Misc
(package! wc-mode)
(package! xclip)
(package! jest)

;; Langs
(package! fennel-mode)
(package! svelte-mode)
(package! yasnippet-snippets)

;; Tools
(package! elpher)
(package! ebib)
(package! erc-hl-nicks)

;; Gemini stuff
(package! visual-fill-column)
;; (package! gemini-mode)

;; Disabled packages
(package! realgud-trepan-ni :disable t)

(package! toolbox-tramp
  :recipe (:host github :repo "fejfighter/toolbox-tramp"))
