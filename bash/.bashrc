#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='ls --color=auto'
alias ls='exa'
alias cat='bat -p'
alias cp='cp --reflink=auto'
alias vim='nvim'
alias e='emacsclient -c --tty'
alias podrun='podman run -it --rm'

alias games='cd /mnt/games'
alias videos='cd /mnt/videos'

PS1='[\u@\h \W]\$ '
[ -r /home/sami/.byobu/prompt ] && . /home/sami/.byobu/prompt   #byobu-prompt#
. ~/.bash_aliases
# >>> Added by cnchi installer
export BROWSER=/usr/bin/firefox
export EDITOR="emacsclient -c -a ''"
stty -ixon

export RUST_SRC_PATH=$(rustc --print sysroot)/lib/rustlib/src/rust/src/
export TERM=xterm-256color

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools

function startgnome() {
    # export MOZ_DBUS_REMOTE=1
    export MOZ_ENABLE_WAYLAND=1
    export XDG_SESSION_TYPE=wayland
    export XDG_CURRENT_DESKTOP=GNOME

    exec dbus-run-session gnome-session
}

function startsway() {
    # export MOZ_DBUS_REMOTE=1
    export MOZ_ENABLE_WAYLAND=1
    export XDG_SESSION_TYPE=wayland
    export XDG_CURRENT_DESKTOP=sway

    systemctl --user import-environment
    exec sway > sway.log
}

source ~/.profile
