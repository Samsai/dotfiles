#! /bin/sh

echo "Stowing configs..."

stow bash
stow vim
stow sway
stow i3
stow i3blocks
stow termite
stow alacritty
stow waybar
stow rofi
stow w3m
stow newsboat
stow doom-emacs

echo "Stow complete."
