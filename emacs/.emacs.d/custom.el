(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cargo-process--custom-path-to-bin "cargo")
 '(cargo-process--rustc-cmd "rustc")
 '(custom-enabled-themes '(modus-vivendi))
 '(custom-safe-themes
   '("87b82caf3ade09282779733fb6de999d683caf4a67a1abbee8b8c8018a8d9a6b"
     "c2c63381042e2e4686166d5d59a09118017b39003e58732b31737deeed454f1c"
     "d4b608d76e3a087b124c74c2b642c933d8121b24e53d4bbd5e7327c36cc69ccc"
     "4343cbc036f09361b2912119c63573433df725f599bfbdc16fb97f1e4847a08b"
     "f12083eec1537fc3bf074366999f0ee04ab23ab3eaba57614785d88b9db2a5d4"
     "841b6a0350ae5029d6410d27cc036b9f35d3bf657de1c08af0b7cbe3974d19ac"
     "7776ba149258df15039b1f0aba4b180d95069b2589bc7d6570a833f05fdf7b6d"
     "79a8c85692a05a0ce0502168bb0e00d25f021a75d8b0136b46978bddf25e3b72"
     "5e05db868f138062a3aedcccefe623eee18ec703ae25d4e5aebd65f892ac5bcc"
     "2459d6e7e96aefaed9cebaf7fde590f64e76c96f48632d8310cfea5d10ec2bb1"
     "9b64a681308383067359cf06bfa6a1bc4fa75c5b68182e4d6ba4d1816277d70e"
     "76c646974f43b321a8fd460a0f5759f916654575da5927d3fd4195029c158018"
     "49887e6f0c666dfc10fad4c23c7a83a176cb296968648c02b85deec25bb11103"
     "e2337309361eef29e91656c5e511a6cb8c54ce26231e11424a8873ea88d9093e"
     "72cc2c6c5642b117034b99dcc3a33ff97a66593429c7f44cd21b995b17eebd4e"
     "578db0ce196a4c849aa995a0dd32d5fe85da59a1ec303614aa40a28bf9ad8b99"
     "0ce768d3ea6d809292d12b22a5ff6de65e24a8d527e4e0885cf87640f42dff7d"
     "33cd1d4d57fdad620c7578ddf7372acb9a7ea106903c152b06781f8554b8e4c9"
     "c01cd0485ce35cf0a19ab91146d2c2b6528ec60ad4c8ffec5b2b7cc4bc05bd80"
     "83fc7ca4d73cee934d2a96d5f2f6fe243afede055497528ab89dbb92c1145020"
     "92cfd42cedb42fdd3ea0d84d518825a94f29b30be20f65978dab7d7c8fa30c6a"
     "d6c5b14073abc649dad816750ef1ac7d5defdf1630d4e9938e06c38ad547d6da"
     "42850fc05cf02fc37c45164a8d7006c762d3ebcd43409efe2c4456a8b89bdf47"
     "af27beda94a2088d5f97c9bcb2bf08a1ede9b4f9562c1b1259ca171fce7d0838"
     "616a43bd873b09e966e837c7138e5b2561b3442b92723d21b8c80166f3ecd9f3"
     "ff1607d931035f2496e58566ee567b44a0f10be2f3f55d8e2956af16a2431d94"
     "857a606b0b1886318fe4567cc073fcb1c3556d94d101356da105a4e993112dc8"
     "4a5aa2ccb3fa837f322276c060ea8a3d10181fecbd1b74cb97df8e191b214313"
     "cf922a7a5c514fad79c483048257c5d8f242b21987af0db813d3f0b138dfaf53"
     "3319c893ff355a88b86ef630a74fad7f1211f006d54ce451aab91d35d018158f"
     "c4063322b5011829f7fdd7509979b5823e8eea2abf1fe5572ec4b7af1dd78519"
     "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63"
     "3263bd17a7299449e6ffe118f0a14b92373763c4ccb140f4a30c182a85516d7f"
     default))
 '(dired-listing-switches "-alh --group-directories-first")
 '(doc-view-image-width 800)
 '(doc-view-resolution 150)
 '(doc-view-scale-internally t)
 '(docker-tramp-docker-executable "podman")
 '(eat-eshell-visual-command-mode t)
 '(eldoc-echo-area-use-multiline-p nil)
 '(ement-notify-notification-predicates
   '(ement-notify--event-mentions-session-user-p
     ement-notify--event-mentions-room-p))
 '(ement-save-sessions t)
 '(erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
 '(erc-modules
   '(completion log notifications spelling hl-nicks netsplit fill button
                match track readonly networks ring autojoin
                noncommands irccontrols move-to-prompt stamp menu list))
 '(erc-nick "Samsai")
 '(evil-undo-system 'undo-redo)
 '(geiser-guile-binary "guile")
 '(geiser-racket-binary "/usr/bin/racket")
 '(org-agenda-files '("~/org"))
 '(org-babel-load-languages '((emacs-lisp . t) (haskell . t) (forth . t) (scheme . t)))
 '(org-babel-python-command "python")
 '(org-export-backends '(ascii html icalendar latex md odt))
 '(org-jira-progress-issue-flow '(("Open" . "In Progress") ("In Progress" . "Done")) t)
 '(org-modules
   '(ol-bbdb ol-bibtex ol-docview ol-doi ol-eww ol-gnus org-habit ol-info
             ol-irc ol-mhe ol-rmail ol-w3m))
 '(package-selected-packages
   '(blacken buffer-env calc-prog-utils cargo consult corfu dashboard
             diredfl dockerfile-mode doom-modeline dumb-jump eat ebib
             editorconfig ef-themes eglot-java elfeed-org elpher ement
             erc-hl-nicks eshell-prompt-extras evil-collection
             evil-surround fennel-mode fic-mode
             flymake-diagnostic-at-point forth-mode geiser-guile
             gemini-mode general gleam-ts-mode go-mode gptel
             graphql-mode haskell-mode inheritenv jenkinsfile-mode
             json-mode leetcode lua-mode magit malyon marginalia
             npm-mode orderless org-bullets org-d20 org-jira
             org-roam-ui org-tree-slide plz-see poetry python-pytest
             rainbow-delimiters restclient rjsx-mode rmsbolt rust-mode
             sly svelte-mode treesit-auto typescript-mode verb vertico
             visual-fill-column vundo web-mode which-key yaml-mode
             yasnippet-snippets))
 '(safe-local-variable-values '((org-d20-party ("Showstopper" . 0))))
 '(tab-width 4)
 '(toolbox-tramp-flatpak-wrap t)
 '(tramp-remote-path
   '(tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin"
                               "/usr/local/bin" "/usr/local/sbin"
                               "/local/bin" "/local/freeware/bin"
                               "/local/gnu/bin" "/usr/freeware/bin"
                               "/usr/pkg/bin" "/usr/contrib/bin"
                               "/opt/bin" "/opt/sbin" "/opt/local/bin"
                               "~/.ghcup/bin" "~/.local/bin"))
 '(truncate-lines t)
 '(typescript-indent-level 2)
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fic-face ((t (:foreground "red" :weight bold)))))
