* Samsai's Emacs config
** Bootstrapping
*** Emacs server startup
#+begin_src emacs-lisp :tangle yes
(require 'server)

(unless (server-running-p)
  (server-start))
#+end_src

*** Handle customizations storing

#+begin_src emacs-lisp :tangle yes
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
(load custom-file))
#+end_src

*** We should also deal with backups
#+begin_src emacs-lisp :tangle yes
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )
#+end_src

*** Bootstrap package management
#+begin_src emacs-lisp :tangle yes
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("nongnu" . "https://elpa.nongnu.org/nongnu/"))

; (setq package-install-upgrade-built-in 't)

(package-initialize)
#+end_src

*** We use use-package to manage packages
#+begin_src emacs-lisp :tangle yes
(unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure 't)
(setq use-package-verbose 't)
#+end_src

*** Custom Functions 
Reloading my configs
#+begin_src emacs-lisp :tangle yes
(defun my/reload-emacs nil
  "Reload Emacs configuration."
  (interactive)
  (load-file "~/.emacs.d/init.el")
  (message "Emacs was reloaded."))
#+end_src

** UI/UX
*** Startup
#+begin_src emacs-lisp :tangle yes
  (use-package dashboard
    :config
    (setq dashboard-startupify-list '(dashboard-insert-banner
                                  dashboard-insert-newline
                                  dashboard-insert-banner-title
                                  dashboard-insert-newline
                                  dashboard-insert-navigator
                                  dashboard-insert-newline
                                  dashboard-insert-init-info
                                  dashboard-insert-items))
    (dashboard-setup-startup-hook))
#+end_src
*** Misc settings
#+begin_src emacs-lisp :tangle yes
(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(when (boundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(setq ring-bell-function 'ignore)
(setq scroll-step 1) ;; keyboard scroll one line at a time
(defalias 'yes-or-no-p 'y-or-n-p)
(hl-line-mode)
(tab-bar-mode)
#+end_src
*** Theming
#+begin_src emacs-lisp :tangle yes
(use-package ef-themes)
#+end_src
*** Modeline
#+begin_src emacs-lisp :tangle yes
(use-package doom-modeline
  :init (doom-modeline-mode 1))
#+end_src
*** Completion
#+begin_src emacs-lisp :tangle yes
  ;; (use-package vertico
  ;;   :init
  ;;   (vertico-mode))

  ;; (use-package orderless
  ;;   :custom
  ;;   (completion-styles '(orderless basic))
  ;;   (completion-category-overrides '((file (styles basic partial-completion)))))

  ;; ;; Enable richer annotations using the Marginalia package
  ;; (use-package marginalia
  ;;   :init
  ;;   (marginalia-mode))

  (use-package icomplete
    :config
    (fido-vertical-mode))

  (use-package consult)

  (use-package which-key
    :config
    (which-key-mode))
#+end_src
*** How I am saving my wrists and fingers
#+begin_src emacs-lisp :tangle yes
(use-package general)

(general-create-definer my-leader-def
  ;; :prefix my-leader
  :prefix "SPC")

(general-create-definer my-local-leader-def
  ;; :prefix my-local-leader
  :prefix "SPC m")

(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (evil-mode 1))

(use-package evil-surround
  :config
  (global-evil-surround-mode))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))
#+end_src
** Environments and terminals

*** Tramp
Basic config
#+begin_src emacs-lisp :tangle yes
  (require 'tramp)

  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
#+end_src

My personal functions for Tramp
#+begin_src emacs-lisp :tangle yes
(defun samsai/strip-tramp-prefix (file)
  (car (last (string-split file ":"))))
#+end_src

Access method for connecting to host from Flatpak
#+begin_src emacs-lisp :tangle yes
(add-to-list 'tramp-methods
             '("fspawnhost"
               (tramp-login-program "flatpak-spawn")
               (tramp-login-args (("--host") ("/bin/bash")))
               (tramp-remote-shell "/bin/bash")
               (tramp-remote-shell-args ("-i" "-c"))))
#+end_src
*** Flatpak overrides
#+begin_src emacs-lisp :tangle yes
(setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "/var/data/local/bin")))
(setq exec-path (append exec-path (list (expand-file-name "/var/data/local/bin"))))

(setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "/var/data/python/bin")))
(setq exec-path (append exec-path (list (expand-file-name "/var/data/python/bin"))))
#+end_src

*** Eat
#+begin_src emacs-lisp :tangle yes
(use-package eat
  :defer t
  :config
  (add-hook 'eshell-load-hook #'eat-eshell-mode)
  (setq eshell-visual-commands nil))

#+end_src
*** Eshell
#+begin_src emacs-lisp :tangle yes
(add-hook 'eshell-mode-hook (lambda () (setenv "TERM" "xterm-256color")))

(defun eshell/lcd (new-path)
  "Switch to a directory relative to the current tramp host"
  (let* ((d default-directory)
         (s (split-string d ":"))
         (path (car (last s)))
         (l (length path))
         (host (substring d 0 (- (length d) l))))
    (eshell/cd (format "%s%s" host new-path))))

;; (add-hook 'eshell-mode-hook
;;           (lambda ()
;;             (setq-local corfu-auto nil)
;;             (corfu-mode)))

(general-def 'insert eshell-mode-map
  "<tab>"  'completion-at-point)

(use-package eshell-prompt-extras :defer t)

(with-eval-after-load "esh-opt"
  (autoload 'epe-theme-lambda "eshell-prompt-extras")
  (setq eshell-highlight-prompt nil
        eshell-prompt-function 'epe-theme-lambda))
#+end_src
*** Buffer environments
#+begin_src emacs-lisp :tangle yes
  (use-package buffer-env
    :config
    (add-hook 'hack-local-variables-hook 'buffer-env-update))

  (use-package inheritenv
    :defer t)
#+end_src
** Programming
*** Misc settings and built-ins
#+begin_src emacs-lisp :tangle yes
  (electric-pair-mode t)

  (setq-default indent-tabs-mode nil)

  (add-hook 'prog-mode-hook #'display-line-numbers-mode)

#+end_src

Highlight fix-mes
#+begin_src emacs-lisp :tangle yes
  (use-package fic-mode
    :hook prog-mode)
#+end_src

Editorconfig support, in case we ever fight over style
#+begin_src emacs-lisp :tangle yes
(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))
#+end_src

Enable rainbow delimiters, helps with Lisps
#+begin_src emacs-lisp :tangle yes
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode)
  :hook (lisp-mode . rainbow-delimiters-mode))

#+end_src

Just in case I need to find a specific undo point
#+begin_src emacs-lisp :tangle yes
(use-package vundo)
#+end_src

Sometimes compile buffer looks ugly with ANSI color codes
#+begin_src emacs-lisp :tangle yes
(use-package ansi-color
    :hook (compilation-filter . ansi-color-compilation-filter)) 
#+end_src

*** General frameworks
**** Completion preview
#+begin_src emacs-lisp :tangle yes
(use-package completion-preview
  :config
  (setq tab-always-indent 'complete)
  (global-completion-preview-mode))
#+end_src
**** Corfu
#+begin_src emacs-lisp :tangle yes
  ;; (use-package corfu
  ;;   :custom
  ;;   (corfu-auto t)          ;; Enable auto completion
  ;;   ;; (corfu-separator ?_) ;; Set to orderless separator, if not using space
  ;;   :bind
  ;;   ;; Another key binding can be used, such as S-SPC.
  ;;   ;; (:map corfu-map ("M-SPC" . corfu-insert-separator))
  ;;   :init
  ;;   (global-corfu-mode))
#+end_src
**** LSP
#+begin_src emacs-lisp :tangle yes
(use-package eglot
  :hook (prog-mode . eglot-ensure))
#+end_src
**** Dumb-jump
#+begin_src emacs-lisp :tangle yes
(use-package dumb-jump
  :init
  (setq xref-show-definitions-function #'xref-show-definitions-completing-read)
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))
#+end_src
**** Treesit
#+begin_src emacs-lisp :tangle yes
(use-package treesit-auto
  :config
  (global-treesit-auto-mode))
#+end_src
**** Flymake
#+begin_src emacs-lisp :tangle yes
(add-hook 'prog-mode-hook #'flymake-mode)

(general-define-key :prefix-map 'project-prefix-map "E" 'flymake-show-project-diagnostics)

(setq help-at-pt-display-when-idle t)

(use-package flymake-diagnostic-at-point
  :after flymake
  :config
  (add-hook 'flymake-mode-hook #'flymake-diagnostic-at-point-mode))
#+end_src
**** Yasnippet
#+begin_src emacs-lisp :tangle yes
  (use-package yasnippet
    :defer t
    :hook (prog-mode . yas-minor-mode)
    :config
    (yas-global-mode 1))

  (use-package yasnippet-snippets
    :after yasnippet)
#+end_src
**** Project.el
#+begin_src emacs-lisp :tangle yes
(defun project-root-override (dir)
  "Find DIR's project root by searching for a '.project.el' file.

If this file exists, it marks the project root. For convenient compatibility
with Projectile, '.projectile' is also considered a project root marker.

https://blog.jmthornton.net/p/emacs-project-override"
  (let ((root (or (locate-dominating-file dir ".project.el")
                  (locate-dominating-file dir ".projectile")))
        (backend (ignore-errors (vc-responsible-backend dir))))
    (when root (if (version<= emacs-version "28")
                    (cons 'vc root)
                  (list 'vc backend root)))))

(use-package project
  ;; Cannot use :hook because 'project-find-functions does not end in -hook
  ;; Cannot use :init (must use :config) because otherwise
  ;; project-find-functions is not yet initialized.
  :config
  ;; Returns the parent directory containing a .project.el file, if any,
  ;; to override the standard project.el detection logic when needed.
  (setq project-vc-extra-root-markers '(".project.el" ".projectile" ))
  (add-hook 'project-find-functions #'project-root-override))
#+end_src
*** Tools
**** Magit
#+begin_src emacs-lisp :tangle yes
  (use-package magit
    :defer t
    :config
    (magit-auto-revert-mode)
    :bind (:map project-prefix-map
                ("m" . magit-project-status)))
#+end_src
**** Restclient
#+begin_src emacs-lisp :tangle yes
  (use-package restclient
    :mode ("\\.rest\\'" . restclient-mode))
#+end_src

#+begin_src emacs-lisp :tangle yes
  (use-package verb)
#+end_src
**** RMSBolt
#+begin_src emacs-lisp :tangle yes
(use-package rmsbolt :defer t)
#+end_src
**** Eglot
#+begin_src emacs-lisp :tangle yes
(use-package eglot
  :config
  (add-to-list 'eglot-server-programs '((js2-mode typescript-ts-mode) . es-server-program)))
#+end_src
**** GPT - for testing work stuff
This config should allow connecting to the Ollama instance inside
Alpaca, so that Alpaca can deal with the difficulties of getting
ROCM to work. Hopefully the port is stable.

#+begin_src emacs-lisp :tangle yes
(use-package gptel
  :config
  (setq
   gptel-model   'granite3.1-moe:3b
   gptel-backend  (gptel-make-ollama "Ollama"
                    :host "localhost:11435"
                    :stream t
                    :models '(granite3.1-dense:2b granite3.1-dense:8b granite3.1-moe:3b))
   gptel-default-mode 'org-mode))
#+end_src
*** Languages
**** Rust
#+begin_src emacs-lisp :tangle yes
  (use-package rust-mode
    :hook (rust-ts-mode . eglot-ensure))

  (use-package cargo
    :after rust-mode)
#+end_src
**** Web langs
***** Svelte
#+begin_src emacs-lisp :tangle yes
  (use-package svelte-mode
    :defer t
    :config
    (add-to-list 'eglot-server-programs '(svelte-mode . ("svelteserver" "--stdin"))))
#+end_src
***** HTML/CSS
#+begin_src emacs-lisp :tangle yes
(use-package web-mode
  :defer t
  :init
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode)))
#+end_src
***** JavaScript/TypeScript
#+begin_src emacs-lisp :tangle yes
(setq js-indent-level 2)

(use-package rjsx-mode
  :mode "\\.jsx\\'"
  :mode "\\.tsx\\'")

(use-package js2-mode
  :mode "\\.js\\'"
  :mode "\\.mjs\\'"
  :config

  (setq js2-strict-missing-semi-warning nil)
  (setq js2-strict-missing-semi-warning nil))

(use-package npm-mode
  :defer t)
#+end_src

Language server configurations for Node and Deno:

#+begin_src emacs-lisp :tangle yes
(defclass eglot-deno (eglot-lsp-server) ()
  :documentation "A custom class for deno lsp.")

(cl-defmethod eglot-initialization-options ((server eglot-deno))
  "Passes through required deno initialization options"
  (list :enable t
        :lint t))

(defun deno-project-p ()
  "Predicate for determining if the open project is a Deno one."
  (let ((p-root (caddr (project-current))))
    (file-exists-p (concat p-root "deno.json"))))

(defun node-project-p ()
  "Predicate for determining if the open project is a Node one."
  (let ((p-root (caddr (project-current))))
    (file-exists-p (concat p-root "package.json"))))

(defun es-server-program (_)
  "Decide which server to use for ECMAScript based on project characteristics."
  (cond ((deno-project-p) '(eglot-deno "deno" "lsp"))
	    ((node-project-p) '("typescript-language-server" "--stdio"))
	    (t                nil)))
#+end_src
***** GraphQL
#+begin_src emacs-lisp :tangle yes
(use-package graphql-mode
  :defer t)
#+end_src
**** Python
Pytest is my test runner of choice
#+begin_src emacs-lisp :tangle yes
  (use-package python-pytest
    :defer t)
#+end_src

We configure the Pytest keybinds here, since that way we can make it defer
#+begin_src emacs-lisp :tangle yes
(use-package python
  :defer t
  :config
  (my-local-leader-def 'normal python-mode-map
    "t"    'python-pytest-dispatch))
#+end_src

Sometimes formatting is easier with tools
#+begin_src emacs-lisp :tangle yes
(use-package blacken
  :defer t)
#+end_src

Poetry is my project manager of choice
#+begin_src emacs-lisp :tangle yes
(use-package poetry
  :defer t)
#+end_src
**** Java
I am using eglot-java in order to handle JDTLS installation and upgrades.

However, it cannot be used at runtime due to incompatibility with Tramp, since
it uses its own override path for JDTLS. I rely on remote access, so I will
rely on the eglot defaults instead.
#+begin_src emacs-lisp :tangle yes
(use-package eglot-java
  :defer t)

;; eglot-java will install JDTLS to the .emacs.d directory
(add-to-list 'tramp-remote-path "~/.emacs.d/share/eclipse.jdt.ls/bin")

;; Gradle will manually install to /opt/gradle
(add-to-list 'tramp-remote-path "/opt/gradle/gradle-8.9/bin/")
#+end_src
**** Go
#+begin_src emacs-lisp :tangle yes
  (use-package go-mode
    :defer t)
#+end_src
**** Haskell
#+begin_src emacs-lisp :tangle yes
(setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "~/.ghcup/bin")))
(setq exec-path (append exec-path (list (expand-file-name "~/.ghcup/bin"))))

(use-package haskell-mode
  :mode "\\.hs\\'"
  )

#+end_src
**** Forth
#+begin_src emacs-lisp :tangle yes
(use-package forth-mode
  :mode "\\.4th\\'"
  :config
  (advice-add 'forth-load-file :filter-args #'samsai/advice-forth-load-file))

(defun samsai/advice-forth-load-file (file &rest rest)
  (cons (samsai/strip-tramp-prefix (car file)) rest))
#+end_src
**** Lisp
***** Guile/Scheme
I'll use Geiser for the development environment

#+begin_src emacs-lisp :tangle yes
(use-package geiser
  :defer t)

(use-package geiser-guile
  :defer t)
#+end_src
***** Common Lisp
Sly seems a bit more modern than SLIME
#+begin_src emacs-lisp :tangle yes
(use-package sly
  :defer t
  :config
  (setq inferior-lisp-program "sbcl"))
#+end_src
***** Fennel
#+begin_src emacs-lisp :tangle yes
(use-package fennel-mode
  :mode "\\.fnl\\'")
#+end_src
**** Lua
Surprisingly we didn't have a mode for this
#+begin_src emacs-lisp :tangle yes
(use-package lua-mode
  :defer t)
#+end_src
**** Gleam
#+begin_src emacs-lisp :tangle yes
(use-package gleam-ts-mode
  :mode "\\.gleam\\'")
#+end_src
**** Config file formats
***** Docker
#+begin_src emacs-lisp :tangle yes
  (use-package dockerfile-mode :defer t)
#+end_src
***** Jenkins
#+begin_src emacs-lisp :tangle yes
(use-package jenkinsfile-mode
  :defer t)
#+end_src
***** Yaml
#+begin_src emacs-lisp :tangle yes
  (use-package yaml-mode :defer t)
#+end_src
***** JSON
#+begin_src emacs-lisp :tangle yes
  (use-package json-mode :defer t)
#+end_src
*** Training
**** Leetcode

#+begin_src emacs-lisp :tangle yes
(use-package leetcode
  :defer t)
#+end_src
** Writing
*** Spelling
#+begin_src emacs-lisp :tangle yes
  ;; find aspell and hunspell automatically
  (cond
   ;; try hunspell at first
   ;; if hunspell does NOT exist, use aspell
   ((executable-find "hunspell")
    (setq ispell-program-name "hunspell")
    (setq ispell-local-dictionary "en_US")
    (setq ispell-local-dictionary-alist
          ;; Please note the list `("-d" "en_US")` contains ACTUAL parameters passed to hunspell
          ;; You could use `("-d" "en_US,en_US-med")` to check with multiple dictionaries
          '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_US") nil utf-8)))

    ;; new variable `ispell-hunspell-dictionary-alist' is defined in Emacs
    ;; If it's nil, Emacs tries to automatically set up the dictionaries.
    (when (boundp 'ispell-hunspell-dictionary-alist)
      (setq ispell-hunspell-dictionary-alist ispell-local-dictionary-alist)))

   ((executable-find "aspell")
    (setq ispell-program-name "aspell")
    ;; Please note ispell-extra-args contains ACTUAL parameters passed to aspell
    (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))))
#+end_src

*** Scientific writing
#+begin_src emacs-lisp :tangle yes
  (use-package ebib
    :defer t)
#+end_src

*** Text appearance
Sometimes it's best to not hard wrap text (for example, when writing Gemtext)
#+begin_src emacs-lisp :tangle yes
  (use-package visual-fill-column
    :defer t)
#+end_src

*** Org-mode
#+begin_src emacs-lisp :tangle yes
(use-package org
  :hook (org-mode . org-indent-mode)
  :config 
  (setq org-src-preserve-indentation t)
  (setq org-todo-keywords
        '((sequence "TODO(t)" "STRT(s)" "|" "DONE(d)")
          (sequence "PROJ(p)" "|")
          (sequence "HOLD(h)" "|")
          (sequence "WAIT(w)" "|")
          (sequence "IDEA(i)" "|")
          (sequence "REVIEW(r)" "|")
          (sequence "|" "KILL(k)")))

  (setq org-todo-keyword-faces
        '(("TODO" . "yellow") ("PROJ" . "purple") ("STRT" . "green") ("HOLD" . "yellow") ("WAIT" . "yellow") ("KILL" . "red") ("IDEA" . "blue") ("REVIEW" . "aquamarine")))

  (setq org-default-notes-file "~/org/todo.org")

  (setq org-capture-templates
        '(("t" "Todo" entry (file+headline "~/org/todo.org" "Inbox")
           "* TODO %?\n  %i\n  %a")
          ("n" "Note" entry (file+datetree "~/org/notes.org")
           "* %?\n"))))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode))

(general-def 'normal org-mode-map
  "<<"      'org-promote-subtree
  ">>"      'org-demote-subtree
  "<tab>"  'org-cycle)

(my-local-leader-def 'normal org-mode-map
  "t"    'org-todo
  "c"    '(:ignore t :which-key "clock")
  "c i"  'org-clock-in
  "c o"  'org-clock-out
  "r"    'org-refile
  "v"    '(:keymap verb-command-map :which-key "verb"))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/org/roam/"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

(use-package org-roam-ui)

(use-package org-tree-slide
  :defer t
  :config (org-tree-slide-simple-profile))
#+end_src

*** Markdown
Sometimes we need to edit MDX, we can use markdown-mode for that
#+begin_src emacs-lisp :tangle yes
(use-package markdown-mode
  :mode "\\.mdx\\'"
  :mode "\\.md\\'"
)
#+end_src
** Apps
*** Elfeed
#+begin_src emacs-lisp :tangle yes
  (use-package elfeed-org :defer t)

  (use-package elfeed
    :defer t
    :init (elfeed-org))

  (setq rmh-elfeed-org-files (list "~/org/elfeed.org"))

  (defun samsai/elfeed-mark-all-as-read ()
    "Mark all entries in Elfeed as read."
    (interactive)
    (mark-whole-buffer)
    (elfeed-search-untag-all-unread))

  (defun samsai/elfeed-show-eww ()
    "Browse elfeed ENTRY in eww"
    (interactive)
    (let ((browse-url-browser-function #'eww-browse-url))
      (elfeed-search-browse-url)))

  (general-def 'normal elfeed-search-mode-map
    "RET"    'elfeed-search-show-entry
    "U"      'elfeed-update
    "b"      'elfeed-search-browse-url
    "e"      'samsai/elfeed-show-eww
    "/"      'elfeed-search-set-filter
    "a"      'elfeed-search-untag-all-unread
    "A"      'samsai/elfeed-mark-all-as-read)
#+end_src
*** Gemini/Elpher
#+begin_src emacs-lisp :tangle yes
  (use-package elpher :defer t)

  (use-package gemini-mode :defer t)
#+end_src

*** Matrix/Ement
#+begin_src emacs-lisp :tangle yes
  (use-package ement :defer t)
#+end_src
*** IRC/ERC
#+begin_src emacs-lisp :tangle yes
(use-package erc-hl-nicks :defer t)

(let ((erc-ignores-file (expand-file-name "erc-ignores.el" user-emacs-directory)))
  (when (file-readable-p erc-ignores-file)
    (load-file erc-ignores-file)))
#+end_src
*** TTRPG
#+begin_src emacs-lisp :tangle yes
  (use-package org-d20
    :defer t)
#+end_src

*** Dired
#+begin_src emacs-lisp :tangle yes
  (use-package dired
    :defer t
    :ensure nil
    :config
    (setq dired-dwim-target t)
    (general-def 'normal dired-mode-map
      "h" 'dired-up-directory
      "l" 'dired-find-file))

  (use-package diredfl
    :after dired
    :init
    (diredfl-global-mode))
#+end_src

*** Calc
#+begin_src emacs-lisp :tangle yes
(use-package calc-prog-utils
  :after calc)
#+end_src
*** JIRA

Oh how I hate thee. May you be less annoying through the lense of Emacs.

#+begin_src emacs-lisp :tangle yes
(use-package org-jira
  :defer t
  :config
  (setq jiralib-url "https://jiradc2.ext.net.nokia.com")
  (setq org-jira-custom-jqls '(
                               (:jql " (project = NMP AND issuetype = Story AND labels = SDK AND (status != Resolved AND status != Closed)) OR (project = 'NACNAC FM' AND Component = NaC_SDK) ORDER BY priority DESC "
                                     :filename "open-sdk-tickets")
                               (:jql " project = NMP AND issuetype = Story AND 'Epic Link' = NMP-741 AND (status != Resolved AND status != Closed) ORDER BY priority DESC "
                                     :filename "kong-poc-tickets")
                               ))
  )

(defconst org-jira-progress-issue-flow
  '(("Open" . "Start Dev")
    ("In Progress" . "Resolve Issue")))

#+end_src
*** Malyon
Z-machine text games in Emacs!

#+begin_src emacs-lisp :tangle yes
(use-package malyon
  :defer t)
#+end_src
** Keymaps (needs reorganizing)
#+begin_src emacs-lisp :tangle yes
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(my-leader-def
  :states '(normal visual motion)
  :keymaps 'override
  "<tab>" '(:keymap tab-prefix-map :which-key "tab")
  "w"     '(:keymap evil-window-map :which-key "window")
  "p"     '(:keymap project-prefix-map :which-key "project")
  "."     'find-file
  "SPC"   'project-find-file
  "b"     '(:ignore t :which-key "buffer")
  "b k"   'kill-current-buffer
  "b b"   'switch-to-buffer
  "c"     '(:ignore t :which-key "code")
  "c a"   'eglot-code-actions
  "c b"   'magit-blame
  "c e"   'flymake-show-buffer-diagnostics
  "c n"   'flymake-goto-next-error
  "c p"   'flymake-goto-previous-error
  "c r"   'eglot-rename
  "s"     '(:ignore t :which-key "snippets")
  "s i"   'yas-insert-snippet
  "+"     'text-scale-increase
  "-"     'text-scale-decrease
  "X"     'org-capture
  "y"     'consult-yank-from-kill-ring)


(my-local-leader-def 'normal latex-mode-map
  "i"    'ebib-insert-citation)

(general-define-key
 :states 'normal
 "gc" 'comment-line
 "K"  'eldoc)

(general-define-key
 :states 'normal
 "gc" 'comment-line
 "K"  'eldoc)

(general-define-key
 :states 'insert
 "<tab>" 'indent-for-tab-command)

(global-set-key [remap dabbrev-expand] 'hippie-expand)

(general-define-key
 :states 'visual
 "<tab>" 'indent-for-tab-command)
#+end_src
